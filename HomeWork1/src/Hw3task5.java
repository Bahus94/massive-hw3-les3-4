public class Hw3task5 {
    public static void main(String[] args) {
        int mass[];
        mass = new int[] {8, 5, 19, 10, 61, 14};
        int sum=0;
        int n=0;

        for(int i=0; i<mass.length; i++)
        {
            if( mass[i]%2-1==0 )
            {
                n++;
                sum += mass[i];

                System.out.println("Нечетный элемент = " + mass[i]);
            }
        }

        System.out.println( "Всего нечетных элементов = " + n );
        System.out.println( "Cумма нечетных элементов = " + sum );
    }
}
